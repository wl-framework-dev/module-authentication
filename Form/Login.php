<?php

	namespace Authentication;

	use \WebLab_Form;
	use \WebLab_Form_Input;

	class Form_Login extends WebLab_Form {

		public function __construct() {
			parent::__construct( '?' );
		}

		public function _setupFields() {
			$this->add( new WebLab_Form_Input( 'target', 'hidden' ) );
			$this->add( new WebLab_Form_Input( 'username', 'text', 'Gebruikersnaam', null, array( 'id' => 'username' ) ) );
			$this->add( new WebLab_Form_Input( 'password', 'password', 'Wachtwoord', null, array( 'id' => 'password' ) ) );
		}

		public function _setupValidation() {
			$username = $this->get('username')->getValue();
			$this->get('password')->addFilter(new Filter_Authentication( 'is_valid', $username ), 'Ongeldige gebruikersnaam/wachtwoord');
		}

	}