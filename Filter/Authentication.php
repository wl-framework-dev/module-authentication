<?php

	namespace Authentication;

	use \WebLab_Filter;

	class Filter_Authentication extends WebLab_Filter {

		public function is_valid( $username, $password ) {
			return Table_Accounts::getInstance()->isValid( $username, $password );
		}

	}