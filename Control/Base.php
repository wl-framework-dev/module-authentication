<?php

	namespace Authentication;

	use \WebLab_Session as Session;
	use Redirect\Redirect;
	use \WebLab_Dispatcher_Module;

	abstract class Control_Base extends WebLab_Dispatcher_Module {

		public function __init() {
			return $this->__auth();
		}

		public function __auth() {
			if( isset( $this->param['authentication'] ) && $this->param['authentication'] == 'login' )
				return true;

			if( Session::contains( 'user_id' ) )
				return true;

			Redirect::go( BASE . 'authentication/login?target=' . urlencode( $_SERVER['REQUEST_URI'] ) );
		}

	}