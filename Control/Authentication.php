<?php

	namespace Authentication;

	use \WebLab_Dispatcher_Module;
	use \WebLab_Template;
	use Redirect\Redirect;
	use \WebLab_Session as Session;
	use \WebLab_Config;

	class Control_Authentication extends WebLab_Dispatcher_Module {

		public function _default() {
			Redirect::go( BASE . 'authentication/login' );
		}

		public function login() {
			$t = $this->layout->content = new WebLab_Template( 'Authentication\auth/login' );
			$form = $t->form = new Form_Login();

			if( !$form->isPostback() ) {
				if( !empty( $_GET['target'] ) ) {
					$target = $_GET['target'];
				} else {
					$config = WebLab_Config::getApplicationConfig()->get('Authentication', WebLab_Config::OBJECT);
					$target = isset( $config->default_target ) ? BASE . ltrim( $config->default_target, '/' ) : BASE;
				}

				$form->get('target')->setValue( $target );
			}

			if( $form->isValid() && $form->isPostback() ) {
				$username = $form->get('username')->getValue();
				$password = $form->get('password')->getValue();
				$user = Table_Accounts::getInstance()->getUser( $username, $password );
				Session::set( 'user_id', $user->uid );
				Redirect::go( $form->get('target')->getValue() );
			}
		}

		public function logout() {
			Session::destroy();
			$this->layout->content = new WebLab_Template('Authentication\auth/logout');
		}

	}