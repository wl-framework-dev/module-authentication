<form action="<?=$form->getAction()?>" method="<?=$form->getMethod()?>">
	<?=$form->postback?>
	<?php if( !$form->isValid() ): ?>
	<ul class="error">
		<?php foreach( $form->getErrors() as $errors ):
			foreach( $errors as $error ): ?>
		<li><?=$error?></li>
	<?php endforeach;
		endforeach; ?>
	</ul>
	<?php endif; ?>
	<?=$form->get('target')?>
	<?php foreach( array( 'username', 'password' ) as $field ): $field = $form->get($field); ?>
	<div class="line">
		<label for="<?=$field->attr('id')?>"><?=$field->getLabel()?></label>
		<?=$field?>
	</div>
	<?php endforeach; ?>
	<div class="line">
		<input type="submit" value="Inloggen" />
	</div>
</form>