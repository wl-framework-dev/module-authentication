# WebLab Framework Authentication package
User authentication and controller protection

## Installation

### Setup a location for your modules
You should create a directory that holds all of your modules in one place for each project you work on.
Once you have a folder specifically for this purpose add it to your project configuration.

For example if you use the `Modules` folder in the root of your application.

```json
{
	"Application": {
		"Loader": {
			...		
			"includePaths": [
				"Application",
				"Modules"
			]
		},
	...
```

### Getting a copy
Next clone this module into the directory.

> git clone https://bitbucket.org/wl-framework-dev/module-authentication.git Authentication

### Configuring the module
The configuration for authentication lives in it's own part of the configuration file, outside
of the `Application` block.

The block into which you should put your configuration is `Authentication`, and you should configure
all 3 fields.

 - database: The name of the database as specified in your `Application` block.
 - table: The table name as it is known in your database.
 - fields: This is a map from internal fieldsnames to the names in your table.

There is an optional field that is not required to be in your configuration file.

 - default_target: This is the target to which users will be redirected after authentication.

 You can simply remove fields you do not use, but be aware that we require `uid`, `username` and `password`
 for correct functioning of the module.

```json
{
	"Application": { ... },

	"Authentication": {
		"database": 	"main",
		"table": 		"user",
		"fields": {
			"uid":			"id",
			"username": 	"username",
			"password": 	"password_hash",
			"active":		"online",
			"deleted":		"deleted"
		},
		"default_target":	"/admin"
	}
}
```

Template directory is layed out without any theme, so if your application uses themes you have to implement your own 
`auth/login` template.

### Enable your controller endpoint
To use the build in controller for authentication you should specify an alias for the
`Authentication` controller to redirect to `Authentication\\Authentication`.

```json
{
	"Application": {
		...		
		"Dispatcher": {
			"Aliasses": {
				"":					"Index",
				"Authentication":	"Authentication\\Authentication"
			},
		...
```

## Usage

### Securing a controller
To secure a controller using this module you simply have it inherit from the abstract class
`Authentication\Control_Base`. Now this controller will go through authentication.

### Current user.
The userid of the current user will be stored in the session. The userid is specified by the uid field
in the configuration.

Current user:

```php
<?php
use WebLab_Session as Session;

if( Session::contains('user_id') )
	$user_id = Session::get('user_id');
```

### Overwrite authentication criteria
It is possible to overwrite authentication behaviour of a controller by overwriting the __auth method in your controller.

This example will only allow users with an ID over 10.

```php
class Control_Admin extends Authentication\Control_Base {
	
	public function __auth() {
		if( Session::contains('user_id') )
			return Session::get('user_id') > 10;

		return false;
	}

}
```
