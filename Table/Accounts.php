<?php

	namespace Authentication;

	use \WebLab_Table;
	use \WebLab_Config;

	class Table_Accounts extends WebLab_Table {

		protected static $_fields = array();

		protected static $_table = null;

		protected static $_db = null;

		protected static $_instance;

		protected static $_fields_map = array();

		public static function getInstance() {
			if( empty( static::$_instance ) )
				self::init();

			return parent::getInstance();
		}

		protected static function init() {
			$config = WebLab_Config::getApplicationConfig()->get('Authentication', WebLab_Config::OBJECT);

			if( empty( $config->database ) || empty( $config->table ) || empty( $config->fields ) )
				throw new WebLab_Exception_Config( 'Incomplete authentication configuration. database, table and fields are required.' );

			static::$_database = $config->database;
			static::$_table = $config->table;
			static::$_fields = array_values( $config->fields );
			static::$_fields_map = $config->fields;
		}

		public static function hash( $value ) {
			return sha1( md5( $value ) );
		}

		public function exists( $username ) {
			$username_field = static::$_fields_map['username'];

			$users = $this->findBy(array(
				$username_field => $username
			));

			if( empty( $users ) )
				return false;

			$users = $users->data();

			foreach( $users as &$user ) {
				$user = $this->_mapFields( $user );
			}

			return $users;
		}

		public function getUser( $username, $password ) {
			$password = self::hash( $password );
			$users = $this->exists( $username );

			if( empty( $users ) )
				return false;

			foreach( $users as $user ) {
				if( $user->password == $password )
					return $user;
			}

			return false;
		}

		public function isValid( $username, $password ) {
			$user = $this->getUser( $username, $password );

			return !empty($user);
		}

		protected function _mapFields( $input ) {
			$keys = array_flip( static::$_fields_map );
			$result = array();

			foreach( $input as $key => $value )
				$result[$keys[$key]] = $value;

			return (object)$result;
		}

	}