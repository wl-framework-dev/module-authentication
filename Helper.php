<?php

	namespace Authentication;

	use \WebLab_Session as Session;

	class Helper {

		protected static $user = null;

		protected static $user_id = null;

		public static function isLoggedIn() {
			if( \WebLab_Parser_URL::getForRequest()->getParameter('authentication') == 'logout' )
				return false;

			$user = self::getUser();
			return !empty( $user );
		}

		public static function getUser() {
			$user_id = self::getUserId();
			if( empty( $user_id ) ) {
				self::$user = false;
				return false;
			}

			$user = Table_Accounts::getInstance()->find( $user_id );

			self::$user = $user;
			return $user;
		}

		public static function getUserId() {
			if( self::$user_id !== null )
				return self::$user_id;

			if( !Session::contains( 'user_id' ) ) {
				self::$user_id = false;
				return false;
			}

			self::$user_id = Session::get( 'user_id' );
			return self::$user_id;
		}

		public static function login( $username, $password ) {
			$user = Table_Accounts::getInstance()->getUser( $username, $password );

			if( empty($user) )
				return false;

			Session::set( 'user_id', $user->uid );

			return true;
		}

	}